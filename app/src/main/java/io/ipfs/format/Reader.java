package io.ipfs.format;

public interface Reader {
    int Read(byte[] bytes);
}
