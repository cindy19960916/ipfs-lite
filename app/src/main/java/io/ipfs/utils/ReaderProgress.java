package io.ipfs.utils;

public interface ReaderProgress extends Progress {
    long getSize();
}
